from imutils.video import WebcamVideoStream
from imutils.video import FPS
from face_recognition_knn import predict, show_prediction_labels_on_image
import imutils
import cv2
import time
import datetime

vs = WebcamVideoStream(src=0).start()
start = time.time()

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

prevTime = 0
feture = 0

model = "trained_knn_model.clf"
t = 0.5

while True:
    frame = vs.read()
    frame = imutils.resize(frame, width=640)

    frame_copy = frame
    frameDetect = frame[90:390, 170:470]    # img[y:y+h, x:x+w]

    curTime = time.time()
    sec = curTime - prevTime
    prevTime = curTime
    features = int(time.time())

    fps = 1/(sec)

    strFps = "FPS : %0.1f" % fps
    print(strFps)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    # Rec in frame for detect face
    cv2.rectangle(frame, (170, 90), (470, 390), (0, 0, 255), 2)

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    # print(timestamp)

    # Draw rectangle on face
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

        # print("x" + str(x))
        # print("y" + str(y))

        # if ((x > 170) and (x < 300) and (y > 90) and (y < 300)) and (features > feture+1):
        if ((x > 170) and (x < 300) and (y > 90) and (y < 300)):
            feture = int(time.time())

            # Dummy face for capture
            dummy_face = frame[y:y+h, x:x+w]
            cv2.imwrite("dummy_face.jpg", dummy_face)

            # Write full frame
            # image_fullframe = "result_fullframe_" + str(timestamp) + ".jpg"
            # cv2.imwrite(image_fullframe, frame)

            # Write face
            # image_face = "result_face_" + str(timestamp) + ".jpg"
            # cv2.imwrite(image_face, frame[y:y+h, x:x+w])
            # print("[CAPTURE] => success!!")

            # predict face
            predictions = predict(
                "dummy_face.jpg", model_path=model, distance_threshold=t)

            # Print results on the console
            for name, (top, right, bottom, left) in predictions:
                print("- Found {} at ({}, {}) to ({}, {})".format(name,
                                                                  left, top, right, bottom))

                cv2.putText(frame, name, (50, 50),
                            cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 2)

                cv2.imwrite("result/" + name + "_" +
                            timestamp + ".jpg", dummy_face)

            # draw face and name in fullframe image
            # show_prediction_labels_on_image()

    cv2.imshow('Video', frame)
    # cv2.imshow('copy', frame_copy)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
vs.stop()
